package pl.jackowski.graphqlapi

import graphql.ErrorType
import graphql.ExceptionWhileDataFetching
import graphql.GraphQLError
import graphql.language.SourceLocation
import java.io.PrintWriter
import java.io.StringWriter
import java.util.*

class GraphQLErrorAdapter(private val error: GraphQLError) : GraphQLError {
    override fun getExtensions(): Map<String, Any>? {
        val extensions: MutableMap<String, Any> = HashMap()
        if (error.extensions != null) {
            extensions.putAll(error.extensions)
        }
        if (error is ExceptionWhileDataFetching) {
            val sw = StringWriter()
            val pw = PrintWriter(sw)
            val exception = error.exception
            exception.printStackTrace(pw)
            val stacktrace = sw.toString()
            extensions["stacktrace"] = stacktrace
            extensions["exception"] = exception.javaClass.name
        }
        return if (extensions.isEmpty()) null else extensions
    }

    override fun getLocations(): List<SourceLocation> {
        return error.locations
    }

    override fun getErrorType(): ErrorType {
        return error.errorType
    }

    override fun getPath(): List<Any>? {
        return error.path
    }

    override fun toSpecification(): Map<String, Any> {
        return error.toSpecification()
    }

    override fun getMessage(): String {
        return if (error is ExceptionWhileDataFetching) error.exception.message!! else error.message
    }

}