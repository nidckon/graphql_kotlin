package pl.jackowski.graphqlapi.resolvers

import com.coxautodev.graphql.tools.GraphQLQueryResolver
import org.springframework.stereotype.Component
import pl.jackowski.graphqlapi.entity.Reservation
import pl.jackowski.graphqlapi.services.ReservationService

@Component
class ReservationResolver(val reservationService: ReservationService) : GraphQLQueryResolver {

    fun reservation(id: Long?, creator: String?, startDate: Long?, endDate: Long?): List<Reservation> {
        return reservationService.getReservations()
                .asSequence()
                .filter { id === null || it.id == id }
                .filter { creator === null || it.creator == creator }
                .filter { startDate === null || it.startDate.time == startDate }
                .filter { endDate === null || it.endDate.time == endDate }
                .toList()
    }
}