package pl.jackowski.graphqlapi.resolvers

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import org.springframework.stereotype.Component
import pl.jackowski.graphqlapi.entity.Reservation
import pl.jackowski.graphqlapi.entity.Room
import pl.jackowski.graphqlapi.services.ReservationService
import pl.jackowski.graphqlapi.services.RoomService
import java.util.*

@Component
class ReservationMutator(val roomService: RoomService, val reservationService: ReservationService): GraphQLMutationResolver {

    fun newReservation(roomId: Long, creator: String, startDate: Long, endDate: Long): Room {
        roomService.validateRoomId(roomId)
        val reservation = Reservation(creator, Date(startDate), Date(endDate))
        reservation.roomId = roomId
        reservationService.newReservation(reservation)
        return roomService.getRoom(roomId)
    }

    fun delReservation(id: Long): Boolean {
        val reservation = reservationService.getReservations().first { it.id == id }
        reservationService.delReservation(reservation)
        return true
    }
}