package pl.jackowski.graphqlapi.resolvers

import com.coxautodev.graphql.tools.GraphQLQueryResolver
import org.springframework.stereotype.Component
import pl.jackowski.graphqlapi.entity.Room
import pl.jackowski.graphqlapi.services.RoomService

@Component
class RoomResolver(val roomService: RoomService) : GraphQLQueryResolver {

    fun room(id: Long?, name: String?, location: String?): List<Room> {
        return roomService.getRooms()
                .filter { id === null || it.id == id }
                .filter { name === null || it.name == name }
                .filter { location === null || it.location == location }
                .toList()
    }
}