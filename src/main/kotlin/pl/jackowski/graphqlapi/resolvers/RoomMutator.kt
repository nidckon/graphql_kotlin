package pl.jackowski.graphqlapi.resolvers

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import org.springframework.stereotype.Component
import pl.jackowski.graphqlapi.entity.Room
import pl.jackowski.graphqlapi.services.RoomService

@Component
class RoomMutator(val roomService: RoomService): GraphQLMutationResolver {

    fun newRoom(name: String, location: String): Room {
        val room = Room(name = name, location = location)
        roomService.newRoom(room)
        return room
    }

    fun delRoom(id: Long): Room {
        val room: Room = roomService.getRooms().first {
            it.id == id
        }
        roomService.delRoom(room)
        return room
    }
}
