package pl.jackowski.graphqlapi.entity

data class Room (
    var id: Long = 0L,
    var name: String,
    var location: String?,
    @Transient
    var reservations: List<Reservation> = ArrayList()
)