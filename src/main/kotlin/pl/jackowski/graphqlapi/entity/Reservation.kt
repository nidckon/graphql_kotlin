package pl.jackowski.graphqlapi.entity

import java.util.*

data class Reservation(
        var creator: String,
        var startDate: Date,
        var endDate: Date,
        var id: Long = 0L,
        var roomId: Long = 0L
)