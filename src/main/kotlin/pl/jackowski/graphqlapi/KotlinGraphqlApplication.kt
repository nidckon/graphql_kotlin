package pl.jackowski.graphqlapi

import graphql.GraphQLError
import graphql.servlet.DefaultGraphQLErrorHandler
import graphql.servlet.GenericGraphQLError
import graphql.servlet.GraphQLErrorHandler
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import java.util.*
import java.util.stream.Collectors
import kotlin.collections.ArrayList


@SpringBootApplication
class KotlinGraphqlApplication {

	@Bean
	fun graphQLErrorHandler(): GraphQLErrorHandler {
		return object : DefaultGraphQLErrorHandler() {
			override fun processErrors(errors: List<GraphQLError>): List<GraphQLError> {
				var clientErrors: List<GraphQLError> = errors.stream()
						.filter { e: GraphQLError? -> isClientError(e) }
						.map { error: GraphQLError? -> GraphQLErrorAdapter(error!!) }
						.collect(Collectors.toList())
				var serverErrors: MutableList<GraphQLError> = errors.stream()
						.filter { e: GraphQLError? -> !isClientError(e) }
						.map { error: GraphQLError? -> GraphQLErrorAdapter(error!!) }
						.collect(Collectors.toList())
				if (serverErrors.isNotEmpty()) {
					serverErrors = ArrayList()
					serverErrors.add(GenericGraphQLError("Internal Server Error(s) while executing query"))
				}
				if (clientErrors.isNotEmpty()) {
					clientErrors = ArrayList()
					clientErrors.add(GenericGraphQLError("Client Error(s) while executing query"))
				}
				val e: MutableList<GraphQLError> = ArrayList()
				e.addAll(clientErrors)
				e.addAll(serverErrors)
				return e
			}
		}
	}
}

fun main(args: Array<String>) {
	runApplication<KotlinGraphqlApplication>(*args)
}
