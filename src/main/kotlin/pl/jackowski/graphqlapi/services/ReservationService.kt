package pl.jackowski.graphqlapi.services

import org.springframework.stereotype.Service
import pl.jackowski.graphqlapi.entity.Reservation

@Service
class ReservationService {
    private val reservations: ArrayList<Reservation> = ArrayList()
    var id: Long = 1L

    @Synchronized
    final fun newReservation(reservation: Reservation) {
        reservation.id = id++
        reservations.add(reservation)
    }

    final fun delReservation(reservation: Reservation) {
        reservations.remove(reservation)
    }

    final fun getReservations(): List<Reservation> = reservations.toList()
}
