package pl.jackowski.graphqlapi.services

import org.springframework.stereotype.Service
import pl.jackowski.graphqlapi.entity.Reservation
import pl.jackowski.graphqlapi.entity.Room
import java.lang.IllegalStateException


@Service
class RoomService(private val reservationService: ReservationService) {
    private val rooms: ArrayList<Room> = ArrayList()
    var id: Long = 1L

    init {
        newRoom(Room(name = "Beta", location = "3 piętro"))
    }

    @Synchronized
    final fun newRoom(room: Room) {
        room.id = id++
        rooms.add(room)
    }

    final fun delRoom(room: Room) {
        rooms.remove(room)
    }

    final fun getRooms(): List<Room> = rooms.toList().map { run {
        it.reservations = refreshReservations(it.id)
        it
    } }

    private fun refreshReservations(id: Long): List<Reservation> =
        reservationService.getReservations().filter { it.roomId == id }.toList()

    fun validateRoomId(roomId: Long) {
        getRooms().first { it.id == roomId }
    }

    fun getRoom(roomId: Long): Room = getRooms().first { it.id == roomId }
}