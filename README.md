# GraphQL
Rozwiązanie pozwalające pisać programowalne zapytania do API.

Połączenie zawsze jest nawiązywane z konkretnym hostem bez względu na cel zapytania.
W zależności od przekazanych danych, rozwiązuje się dopasowanie do odpowiedniego `Resolvera`  bądź `Mutatora` i tam wykonywana jest logika.

W zależności od ustalonego wcześniej [schematu](./src/main/resources/room.graphqls) (oraz odpowiednio skonstruowanej klasy rozwiązującej zapytanie) jesteśmy w stanie pozyskać informacje w takiej formie, jakiej aktualnie potrzebujemy.

# Projekt
Aktualny projekt jest przygotowany w celu sprawdzenia i przedstawienia działania samego GraphQL oraz możliwości obsługi logiki.
Wykorzystuje gotową zależność `GraphIQL` w celu symulacji klienta.
Endpointy:
- [client](http://localhost:9000/client) `http://localhost:9000/client` - służy do testowania zapytań, sprawdzenia napisanych schematów
- [api](http://localhost:9000/api) `http://localhost:9000/api` - służy do komunikacji z serwerem

# Query
Zapytania query służą do uzyskania danych, które potrzebujemy. Mogą one wyglądać następująco:
```graphql
type Query {
    room(id: ID, name: String, location: String): [Room]
}
```
Widzimy, że zapytanie powyżej zwraca listę obiektów typu `Room`. W schematach dla GraphQL wszystkie typy (poza podstawowymi) należy zdefiniować.

# Własny Typ
Zdefiniowany typ, zwracany przez zapytanie, wygląda następująco:
```graphql
type Room {
    id: ID!
    name: String
    location: String
}
```
W powyższej deklaracji typu `Room` wskazujemy wprost, że jedynym wymagalnym do zwrócenia polem jest `ID`. Pozostałe pola:
- `name`,
- `location`

Nie są wymagane do zwrócenia, nie musimy o nie pytać i nie pojawią się one w odpowiedzi.

# CRUD
W celu wykonania operacji innych, niż pozyskanie danych, należy zdefiniować `mutacje`, czyli zapytania, które modyfikują dane.

```graphql
type Mutation {
    newRoom(name: String!, location: String!): Room
    delRoom(id: ID!): Room
}
```
W powyższym fragmencie widzimy 2 różne mutacje:
- `newRoom` - która posłuży nam do utworzenia nowego obiektu typu `Room` (opisanego wcześniej). Wymagane pola:
   - `name`
   - `location`
- `delRoom` - która będzie usuwać nasze obiekty po zadanym `ID`. `ID` jest również jedynym możliwym i wymagalnym polem.

# Lista zapytań
## Lista aktywnych salek
```graphql
{
  room {
    id
    name
    location
    reservations {
      id
      creator
      startDate
      endDate
    }
  }
}
```
```bash
curl 'http://localhost:9000/api' \
-H 'Accept: application/json' \
-H 'Content-Type: application/json' \
--data-binary '{"query":"{room {id name location reservations {id creator startDate endDate}}}"}' && echo
```
## Dodanie nowej salki
```graphql
mutation {
	newRoom(name: "Alfa", location: "3 piętro") {
    id
  }
}
```
```bash
curl 'http://localhost:9000/api' -H 'Accept: application/json' -H 'Content-Type: application/json' --data-binary '{"query":"mutation {newRoom(name: \"Alfa\", location: \"3 piętro\") {id}}"}' && echo
```
## Dodanie rezerwacji do istniejącej salki
```graphql
mutation {
  newReservation(roomId: 1, creator: "Patryk Jackowski", startDate: "1579770000000", endDate: "1579773600000") {
    id
  }
}
```
```bash
curl 'http://localhost:9000/api' -H 'Accept: application/json' -H 'Content-Type: application/json' --data-binary '{"query":"mutation {newReservation(roomId: 1, creator: \"Patryk Jackowski\", startDate: \"1579770000000\", endDate: \"1579773600000\") {id}}"}' && echo
```
## Usunięcie salki
```graphql
mutation {
  delRoom(id: 2) {
    id
  }
}
```
```bash
curl 'http://localhost:9000/api' -H 'Accept: application/json' -H 'Content-Type: application/json' --data-binary '{"query":"mutation {delRoom(id: 2) {id}}"}' && echo
```
## Usunięcie rezerwacji
```graphql
mutation {
  delReservation(id: 1)
}
```
```bash
curl 'http://localhost:9000/api' -H 'Accept: application/json' -H 'Content-Type: application/json' --data-binary '{"query":"mutation {delReservation(id: 1)}"' && echo
```

# Wyjątki
Istnieje możliwość przechwycenia wyjątków zwracanych przez GraphQL oraz przez nas. Dzięki takiemu zabiegowi, możemy je odlogować, przetworzyć oraz dodać dodatkowe dane do błędu (np. czas), albo też zwrócić ogólny wyjątek z informacją, że to nie jest błąd po stronie serwera.

Aby dokonać obsługi, należy utworzyć Bean `fun graphQLErrorHandler(): GraphQLErrorHandler` w naszej [aplikacji](./src/main/kotlin/pl/jackowski/graphqlapi/KotlinGraphqlApplication.kt). 
Dodatkowo można stworzyć `adapter` aby wrappować otrzymane wyjątki i mieć lepszy dostęp do danych. Przykładowy adapter znajdziemy [tutaj](./src/main/kotlin/pl/jackowski/graphqlapi/GraphQLErrorAdapter.kt).

Przykładowy wyjątek przed obsługą:
![Before Handling](./doc/OldExceptionResponse.png)

Przykładowy wyjątek po obsłużeniu:
![Before Handling](./doc/NewExceptionResponse.png)

# Źródła
- GraphQL: [graphql.org](https://graphql.org/)
- ExceptionHandler: [picodotdev.github.io](https://picodotdev.github.io/blog-bitix/2017/11/devolver-mensajes-de-error-descriptivos-en-graphql/)
- GraphQL - kotlin i springboot: [auth0.com](https://auth0.com/blog/building-graphql-apis-with-kotlin-spring-boot-and-mongodb/)
- GraphQL vs Rest [howtographql.com](https://www.howtographql.com/basics/1-graphql-is-the-better-rest/)
